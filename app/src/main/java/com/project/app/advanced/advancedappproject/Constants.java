package com.project.app.advanced.advancedappproject;

import java.util.Random;

/**
 * Constants contains constants used by the app.
 */
public class Constants {
    public static final String SERVER_URL = "https://ihungr-node-server.herokuapp.com/";
    public static final String CHAT_SERVER_URL = "https://ihungr-chat-server.herokuapp.com/";
    private static final String[] STRINGS = {
            "Foodies! Assemble!",
            "Trying to find more hungry people...",
            "Only if food could teleport...",
            "GET INTO MAH BELLY!",
            "I don't need food! Food needs me!",
            "The Hunger Games 4: I'm hungry!",
            "GET /api/food ... I wish....",
            "Square eggs! Where can I get those?",
            "If there is no food, go for cannibalism!",
            "My favorite food is bits.. get it? GET IT?",
            "An android eats IOS for breakfast!",
            "01100110 01101111 01101111 01100100 ! FOOD!",
            "66 6f 6f 64 ! FOOD!",
            "I ate out yesterday. Where? Your mom! YEAH BOI!",
            "I'm sorry we don't have the location for your parents' home.. or do we...",
            "I'm so EGGcited!",
            "Want to TACO 'bout it? Nah It's NACHO problem!",
            "I lost my appetite in NOMNOMNOM!",
            "It feels like you don't CARROT all!!",
            "LETTUCE get a move on already! I'm starving!",
            "Frankly, I just don't give a HAM!",
            "HELLO! Is it BRIE you're looking for?",
            "I'm feeling quite Hungary!",
            "Lets do this again, I'd love to ketchup!",
            "I'm kind of a big dill...",
            "Asian? Indian? I can suggest it all...."
    };

    public String getRandomLoadingString() {
        Random rand = new Random();
        int index = rand.nextInt(STRINGS.length);

        return STRINGS[index];
    }

}
