package com.project.app.advanced.advancedappproject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * MatchMakingActivity requests the friends list of the user from facebook.
 * It then send the information to ihungr node server and gets a session_id back.
 * This is done asynchronously in the background while music and animation is played.
 */
public class MatchMakingActivity extends AppCompatActivity {
    private MediaPlayer m_mediaPlayer;
    private JSONObject m_dataToServer;
    private boolean m_ERROR = false;

    private ArrayList<String> foodChoices;
    private JSONObject restaurant;

    SharedPreferences m_sp;

    /**
     * onCreate hides the action bar, start the music and burger
     * animation with a custom message.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_match_making);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        // get preferred food from foodDialog
        foodChoices = getIntent().getStringArrayListExtra("foodChoices");
        Log.d("foodChoices", foodChoices.toString());
        m_sp = PreferenceManager.getDefaultSharedPreferences(getBaseContext());

        final Constants c = new Constants();

        TextView loadingText = (TextView) findViewById(R.id.loadingString);
        String text = c.getRandomLoadingString();
        loadingText.setText(text);

        AnimationDrawable animation = new AnimationDrawable();
        animation.addFrame(getResources().getDrawable(R.drawable.burgerdance1), 200);
        animation.addFrame(getResources().getDrawable(R.drawable.burgerdance2), 200);
        animation.setOneShot(false);

        ImageView imageAnim =  (ImageView) findViewById(R.id.burger_dance);
        imageAnim.setBackgroundDrawable(animation);

        // start the animation!
        animation.start();

        // Music: http://www.bensound.com
        m_mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.bensound_theelevatorbossanova);

        getFacebookFriends(this, Profile.getCurrentProfile());

    }

    @Override
    protected void onStart() {
        super.onStart();
        Boolean m_play_sound = m_sp.getBoolean("sound", false);
        if(m_play_sound){
            m_mediaPlayer.start();
        }
    }

    @Override
    public void onDestroy() {
        if (m_mediaPlayer != null) {
            m_mediaPlayer.release();
            m_mediaPlayer = null;
        }
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        // Do nothing on back pressed
    }

    // Check if network is available
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Get Facebook friends (using the app) for the current user
     */
    private void getFacebookFriends(final Activity activity, final Profile profile) {
        if (!isNetworkAvailable()) {
            Toast.makeText(this, "No network available", Toast.LENGTH_LONG).show();
        } else if (profile == null) {
            Toast.makeText(this, "Not logged in", Toast.LENGTH_LONG).show();
        } else {
            new GraphRequest(
                    AccessToken.getCurrentAccessToken(),
                    "/me/friends",
                    null,
                    HttpMethod.GET,
                    new GraphRequest.Callback() {
                        public void onCompleted(GraphResponse response) {
                            // JSONObject sent to server
                            JSONObject jsonObject = new JSONObject();

                            // JSONArray for friends list - Sent to server with JSONObject
                            JSONArray jsonArray = new JSONArray();
                            JSONArray food = new JSONArray();
                            try {
                                // Add current profile name and id to JSONObject, sent to server
                                jsonObject.put("name", profile.getName());
                                jsonObject.put("id", profile.getId());
                                jsonObject.put("longitude", "0");
                                jsonObject.put("latitude", "0");
                                for (int i = 0; i < foodChoices.size(); i++) {

                                    food.put(foodChoices.get(i));
                                }
                                jsonObject.put("foodChoices", food);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            // JSONObject containing FB friends from GraphResponse
                            JSONObject fbFriendsObject = response.getJSONObject();
                            try {
                                // JSONArray containing name and id of each FB friend
                                JSONArray fbFriendsArray = (JSONArray) fbFriendsObject.get("data");

                                // Add each FB friend name and id to string list
                                for (int i = 0; i < fbFriendsArray.length(); i++) {
                                    JSONObject row = fbFriendsArray.getJSONObject(i);
                                    row.put("longitude", "0");
                                    row.put("latitude", "0");
                                    jsonArray.put(row);
                                }

                                // Add JSONArray of friends to JSONObject, sent to server
                                jsonObject.put("friends", jsonArray);
                                // Send JSONObject to server
                                m_dataToServer = jsonObject;
                                new Handler().postDelayed(m_sendToServer, 5000);
                            } catch (JSONException e) {
                                m_ERROR = true;
                                Log.d("MMA fbfriends", e.getMessage());
                            }
                        }
                    }
            ).executeAsync();
        }
    }

    /**
     * m_sendToServer starts the asynchronous MatchMakingTask.
     * Using this to add a 5 sec delay for the animation.
     */
    private final Runnable m_sendToServer = new Runnable() {
        @Override
        public void run() {
            new MatchMakingTask().execute(m_dataToServer);
        }
    };

    /**
     * MatchMakingTask sends the ihungr server the friendslist and gets
     * back as a response the session id.
     */
    class MatchMakingTask extends AsyncTask<JSONObject, Void, String> {

        @Override
        protected String doInBackground(JSONObject... params) {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, params[0].toString());

            Request request = new Request.Builder()
                    .url(Constants.SERVER_URL + "api/hungry")
                    .post(body)
                    .build();
            String sessionID = "";
            try {
                Response response = client.newCall(request).execute();
                String jsonData = response.body().string();
                Log.v("MatchMaking, api/hungry", jsonData);
                JSONObject Jobject = new JSONObject(jsonData);
                sessionID = Jobject.getString("session_id");
                restaurant = Jobject.getJSONObject("restaurant");

            } catch (IOException e) {
                m_ERROR = true;
                Log.d("MMA send server", e.getMessage());
            } catch (JSONException e) {
                Log.d("MMA send server", e.getMessage());
                m_ERROR = true;
            }
            return sessionID;
        }
        @Override
        protected void onPostExecute(String result) {
            if(m_ERROR){
                Toast.makeText(getApplicationContext(), "Match making error! please try again", Toast.LENGTH_SHORT).show();
                finish();
            }
            else {
                Intent intent = new Intent(MatchMakingActivity.this, SessionActivity.class );
                intent.putExtra("sessionID", result);
                intent.putExtra("restaurant", restaurant.toString());
                startActivity( intent );
                finish();
            }
        }

    }
}