package com.project.app.advanced.advancedappproject;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;

/**
 * MyApplication maintains global application state.
 */
public class MyApplication extends Application {

    /**
     * onCreate initializes Parse.
     */
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, "nPnaSc0ycVHK70P5h5w9JmJ504kMiXm1nQTag6O5", "z3tYwnNwe3ioPecavQcN4G5hgu7bIBhjtkNYG1Bt");
        ParseInstallation.getCurrentInstallation().saveInBackground();
    }
}
