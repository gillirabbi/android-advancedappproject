package com.project.app.advanced.advancedappproject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * CustomMenuAdapter is the adapter used to populate the
 * list view containing restaurant menu in a session
 */
public class CustomMenuAdapter extends BaseAdapter {
    private final Context m_context;
    private final String[] m_names;
    private final String[] m_prices;
    private final String[] m_descriptions;
    private static LayoutInflater m_layoutInflater = null;

    private Vibrator m_vibrator;
    private Boolean m_use_vibrator = false;
    SharedPreferences m_sp;

    public CustomMenuAdapter(Activity activity, String[] names, String[] prices, String[] descriptions) {
        m_context = activity;
        m_names = names;
        m_prices = prices;
        m_descriptions = descriptions;
        m_layoutInflater = (LayoutInflater)m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return m_names.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView nameView;
        TextView priceView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        m_sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        m_vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        m_use_vibrator = m_sp.getBoolean("vibrate", false);

        Holder holder = new Holder();
        View rowView;
        rowView = m_layoutInflater.inflate(R.layout.menu_list, null);

        holder.nameView = (TextView) rowView.findViewById(R.id.name);
        holder.priceView = (TextView) rowView.findViewById(R.id.price);
        holder.nameView.setText(m_names[position]);
        holder.priceView.setText(m_prices[position]);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(m_context, m_descriptions[position], Toast.LENGTH_LONG).show();
                vibrate();
                new AlertDialog.Builder(m_context)
                        .setTitle(m_names[position])
                        .setMessage(m_descriptions[position] + "\n" + m_prices[position])
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                vibrate();
                                // continue with delete
                            }
                        })
                        .show();
            }
        });

        return rowView;
    }

    public void vibrate() {
        if ( m_use_vibrator ) {
            m_vibrator.vibrate( 300 );
        }
    }
}
