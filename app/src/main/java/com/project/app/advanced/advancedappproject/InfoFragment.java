package com.project.app.advanced.advancedappproject;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.facebook.Profile;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * InfoFragment is used to display who have been matched and their location.
 */
public class InfoFragment extends Fragment {

    private String m_sessionInfo;
    private String m_sessionID;
    private String m_restaurantString;
    private JSONObject m_restaurant;

    private ListView m_listView;
    private CustomListAdapter m_adapter;
    private String m_userFacebookName = Profile.getCurrentProfile().getName();

    private SupportMapFragment m_mapfragment;

    ArrayList<String> m_fbFriendsID = new ArrayList<>();
    ArrayList<String> m_fbFriendsName = new ArrayList<>();
    private boolean fbFriends_Initialized;
    private ArrayList<Marker> m_markers = new ArrayList<>();
    private boolean m_stop =false;

    class InfoTask extends AsyncTask<Void, String, Void> {
        OkHttpClient m_http_client = new OkHttpClient();

        @Override
        protected Void doInBackground(Void... params) {
            while(!m_stop){
                Request request = new Request.Builder()
                        .url(Constants.SERVER_URL + "api/session/" + m_sessionID)
                        .build();

                String result = null;
                try {
                    Response response = m_http_client.newCall(request).execute();
                    result = response.body().string();
                    Log.v("InfoFragment response", result);
                } catch (IOException e) {
                    Log.v("SessionTask", e.getMessage());
                }

                publishProgress(result);
                try {
                    Thread.sleep(5000); //wait 5 sec to ask for update
                } catch (InterruptedException e) {
                    Log.d("SessionTask", e.getMessage());
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            try {
                updateSession(values[0]);
            } catch (JSONException e) {
                Log.d("UpdateSession", e.getMessage());
            }
        }
    }

    private void initialize_fbFriends(JSONArray fbArray) throws JSONException {
        for (int i = 0; i < fbArray.length(); i++) {
            JSONObject row = fbArray.getJSONObject(i);
            if (!row.getString("facebookID").equals(Profile.getCurrentProfile().getId())) {
                m_fbFriendsID.add(row.getString("facebookID"));
                m_fbFriendsName.add(row.getString("name"));
            }
        }
        fbFriends_Initialized = true;
    }

    private int checkUserMarker(JSONObject user) {
        for (int i = 0; i < m_markers.size(); i++) {
            try {
                if (user.getString("name").equals(m_markers.get(i).getTitle())) {
                    // user has a marker, return the index
                    return i;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        // user name not found in markers
        return -1;
    }

    void updateSession(String sessionInfo) throws JSONException {
        if(sessionInfo != null && !sessionInfo.equals(m_sessionInfo)){
            m_sessionInfo = sessionInfo;
            JSONObject jsonObject = new JSONObject(sessionInfo);
            JSONArray users = jsonObject.getJSONArray("users");
            Log.v("Users", users.toString());
            if (!fbFriends_Initialized) {
                initialize_fbFriends(users);
            }
            boolean userAlreadyIn = false;
            for (int i = 0; i < users.length(); i++) {
                JSONObject row = users.getJSONObject(i);
                for (int x = 0; x < m_fbFriendsName.size(); x++) {
                    if (m_fbFriendsName.get(x).equals( row.getString("name") )
                        || m_fbFriendsName.get(x).equals(m_userFacebookName)) {
                        // if user is already in the array or if it is the current user
                        userAlreadyIn = true;
                    }
                }
                // this ensures that the users will be unique and not the current user
                if (!userAlreadyIn) {
                    m_fbFriendsID.add(row.getString("facebookID"));
                    m_fbFriendsName.add(row.getString("name"));
                }
            }
            // get IDs from JSON
            String[] ids = new String[m_fbFriendsID.size()];
            ids = m_fbFriendsID.toArray(ids);

            String[] names = new String[m_fbFriendsName.size()];
            names = m_fbFriendsName.toArray(names);

            m_adapter = new CustomListAdapter(getActivity(), names, ids);
            m_listView.setAdapter(m_adapter);



            int markerPos;
            for (int i = 0; i < users.length(); i++) {
                JSONObject row = users.getJSONObject(i);
                // check if lat/lon is 0 (not set)
                if (row.getDouble("latitude") != 0 && row.getDouble("longitude") != 0) {
                    if ((markerPos = checkUserMarker(row)) == -1) {
                        // user does not have a marker, create a new one
                        m_markers.add(m_mapfragment.getMap().addMarker(
                                        new MarkerOptions()
                                                .position(new LatLng(row.getDouble("latitude"), row.getDouble("longitude")))
                                                .title(row.getString("name"))
                                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)))
                        );
                    } else {
                        // gets the correct position and updates the marker position
                        m_markers.get(markerPos)
                                .setPosition(new LatLng(row.getDouble("latitude"), row.getDouble("longitude")));
                    }
                }
            }

            // if restaurant is available, add marker
            if (m_restaurant != null) {
                m_markers.add(m_mapfragment.getMap().addMarker(
                        new MarkerOptions()
                                .position(new LatLng(m_restaurant.getDouble("latitude"), m_restaurant.getDouble("longitude")))
                                .title(m_restaurant.getString("name"))
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE)))
                );
            }
            //Calculate the markers to get their position
            if (m_markers.size() != 0) {
                LatLngBounds.Builder b = new LatLngBounds.Builder();
                for (Marker m : m_markers) {
                    b.include(m.getPosition());
                }
                Log.d("builder b", b.toString());
                LatLngBounds bounds = b.build();
                //Change the padding as per needed
                Log.v("InfoFrag cameraUpdate", "updating camera to " + bounds.toString());
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, 25, 25, 0);
                m_mapfragment.getMap().animateCamera(cu);
            }
        }
    }

    /**
     * Create a new instance of InfoFragment
     */
    static InfoFragment newInstance(String sessionID, String restaurant) {
        InfoFragment f = new InfoFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("sessionID", sessionID);
        args.putString("restaurant", restaurant);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
            new InfoTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new InfoTask().execute();
        }
        fbFriends_Initialized = false;

        FragmentManager fm = getChildFragmentManager();
        m_mapfragment = (SupportMapFragment) fm.findFragmentById(R.id.fragment_container);
        if (m_mapfragment == null) {
            m_mapfragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.fragment_container, m_mapfragment).commit();
        }

        //Get sessionID
        m_sessionID = getArguments() != null ? getArguments().getString("sessionID") : "1";
        //Get restaurant object

        if (getArguments() != null) {

            if ((m_restaurantString = getArguments().getString("restaurant")) != null) {
                try {
                    Log.d("m_restaurantString", m_restaurantString);
                    m_restaurant = new JSONObject(m_restaurantString);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    @Override
    public void onDestroy() {
        m_stop = true;
        super.onDestroy();

    }

    /**
     * The Fragment's UI is just a simple text view showing its
     * instance number.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_session, container, false);
        m_listView = (ListView) v.findViewById(R.id.friendList);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

}
