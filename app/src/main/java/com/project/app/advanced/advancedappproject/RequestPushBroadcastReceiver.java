package com.project.app.advanced.advancedappproject;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * RequestPushBroadcastReceiver handles push notifications from Parse server.
 */
public class RequestPushBroadcastReceiver extends ParsePushBroadcastReceiver {

    /**
     * When a push notification is opened by the user this method will
     * extract from the notification data object the session_id and
     * forward it to the SessionActivity.
     */
    @Override
    public void onPushOpen(Context context, Intent intent) {

        //To track "App Opens"
        ParseAnalytics.trackAppOpenedInBackground(intent);

        //Here is data you sent
        Log.i("ParseReceiver", intent.getExtras().getString( "com.parse.Data" ));

        String sessionID = "";
        try {
            JSONObject jsonObject = new JSONObject(intent.getExtras().getString( "com.parse.Data" ));
            sessionID = jsonObject.getString("session_id");
        } catch (JSONException e) {
            Log.d("RPBR", "RequestPushBroadcastReceiver: " + e.getMessage());
        }

        Intent i = new Intent(context, SessionActivity.class);
        i.putExtras(intent.getExtras());
        i.putExtra("sessionID", sessionID);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(i);
    }

}
