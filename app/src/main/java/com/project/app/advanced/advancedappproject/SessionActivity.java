package com.project.app.advanced.advancedappproject;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * SessionActivity is used to display the session created by the server
 * in response to a hungry request.
 */
public class SessionActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {
    SessionFragmentAdapter m_adapter;
    ViewPager m_pager;

    String m_sessionID;
    String m_restaurant;

    private String m_userFacebookID;
    // google map things
    private double m_currentLatitude = 0;
    private double m_currentLongitude = 0;

    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private GoogleMap mMap;
    private Location mLastLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_pager);
        FacebookSdk.sdkInitialize(getApplicationContext());

        //Get sessionID
        m_sessionID = getIntent().getStringExtra("sessionID");
        //Get restaurant
        m_restaurant = getIntent().getStringExtra("restaurant");

        //If user is not logged in.
        if(Profile.getCurrentProfile() == null){
            Log.d("IF", "Profile null");
            Intent intent = new Intent(this, LoginActivity.class);
            intent.putExtra("sessionID", m_sessionID);
            startActivity(intent);
            this.finish();
        } else {
            m_adapter = new SessionFragmentAdapter(getSupportFragmentManager());
            
            m_pager = (ViewPager)findViewById(R.id.pager);
            m_pager.setAdapter(m_adapter);
            m_pager.setOffscreenPageLimit(2);
            m_pager.setCurrentItem(1);

            m_adapter.upDateSession(m_sessionID, m_restaurant);

            /** everything below this is all google maps stuff */

            // connect to the google API
            buildGoogleApiClient();
            // Create the LocationRequest object
            mLocationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                    .setFastestInterval(5 * 1000) // 1 second, in milliseconds
                    .setSmallestDisplacement(10);
        }
    }

    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
    }
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }

    /** when the location of the user changes **/
    @Override
    public void onLocationChanged(Location location) {
        Log.d("onLocationChanged", location.toString());
        m_currentLatitude = location.getLatitude();
        m_currentLongitude = location.getLongitude();

        Log.d("SessionAct onLocChanged", m_currentLatitude + " WORKS " + m_currentLongitude + "");
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
            new LocationTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new LocationTask().execute();
        }
    }
    /** when the map has been rendered **/
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
    }
    /** when the user has connected to the google maps api **/
    @Override
    public void onConnected(Bundle bundle) {
        m_userFacebookID = Profile.getCurrentProfile().getId();
        if ( ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED ) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
        }
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        } else {
            //If everything went fine lets get latitude and longitude
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    mGoogleApiClient);
            if (mLastLocation != null) {
                m_currentLatitude = mLastLocation.getLatitude();
                m_currentLongitude = mLastLocation.getLongitude();
            }
            // START RECEIVING ONLOCATIONCHANGED
            startLocationUpdates();
        }
    }
    @Override
    public void onConnectionSuspended(int i) {
        Log.d("SessionActivity", "onConnectionSuspended called with i = " + i);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.e("maps connectionFailed", connectionResult.toString());
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                // The next two lines tell the new client that “this” current class will handle connection stuff
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                        //fourth line adds the LocationServices API endpoint from GooglePlayServices
                .addApi(LocationServices.API)
                .build();
    }
    @Override
    public void onResume() {
        super.onResume();
        //Now lets connect to the API
        Log.d("onResume", "connecting to GoogleAPIClient");
        mGoogleApiClient.connect();
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.v(this.getClass().getSimpleName(), "onPause()");
        //Disconnect from API onPause()
        if (mGoogleApiClient.isConnected()) {
            stopLocationUpdates();
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * LocationTask sends the the location of the user in the background.
     */
    class LocationTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("session_id", m_sessionID);
                jsonObject.put("fb_id", m_userFacebookID);
                jsonObject.put("longitude", m_currentLongitude);
                jsonObject.put("latitude", m_currentLatitude);
            } catch (JSONException e) {
                Log.d("SessionTask", e.getMessage());
            }
            Log.d("LocationTask", "curretLon = " + m_currentLongitude + ", currentLan = " + m_currentLatitude);
            RequestBody body = RequestBody.create(JSON, jsonObject.toString());

            Request post = new Request.Builder()
                    .url(Constants.SERVER_URL + "api/location")
                    .post(body)
                    .build();
            try {
                new OkHttpClient().newCall(post).execute();
            } catch (IOException e) {
                Log.d("SessionTask", e.getMessage());
            }
            return null;
        }
    }
}
