package com.project.app.advanced.advancedappproject;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.facebook.FacebookSdk;

/**
 * SplashActivity displays the splash screen on start up.
 */
public class SplashActivity extends AppCompatActivity {

    private final Handler m_activityHandler = new Handler();

    /**
     * onCreate hides the action bar and starts the spinning burger animation.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        FacebookSdk.sdkInitialize(getApplicationContext());

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        ImageView burgerTime = (ImageView) findViewById(R.id.burger_splash);

        RotateAnimation r = new RotateAnimation(0.0f, -10.0f * 360.0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        r.setDuration((long) 2*1500);
        r.setRepeatCount(0);
        burgerTime.startAnimation(r);

        m_activityHandler.removeCallbacks(m_startLoginActivityRunnable);
        m_activityHandler.postDelayed(m_startLoginActivityRunnable, 2000);
    }

    /**
     * m_startLoginActivityRunnable starts the login activity.
     */
    private final Runnable m_startLoginActivityRunnable = new Runnable() {
        @Override
        public void run() {
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity( intent );
            finish();
        }
    };
}
