package com.project.app.advanced.advancedappproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.widget.ProfilePictureView;

/**
 * MainActivity displays the main screen.
 * Main Screen contains an action bar, profile picture,
 * welcome message and a hungry button.
 * Food preference popup is displayed when the "I'm hungry"
 * button is clicked.
 */
public class MainActivity extends AppCompatActivity {

    private TextView m_textDetails;
    private CallbackManager m_callbackManager;
    private AccessTokenTracker m_tokenTracker;
    private ProfileTracker m_profileTracker;
    private ProfilePictureView m_profileImage;

    private Vibrator m_vibrator;
    private Boolean m_use_vibrator = false;
    SharedPreferences m_sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);
        m_textDetails = (TextView) findViewById(R.id.text_details); // TextView for welcome message
        m_profileImage = (ProfilePictureView) findViewById(R.id.profilePicture); // PictureView for profile image

        m_vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        m_sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        m_callbackManager = CallbackManager.Factory.create();

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        m_tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                Log.d("iHungr", "" + currentAccessToken);
            }
        };

        m_profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if (currentProfile == null) {
                    Intent intent = new Intent(MainActivity.this, LoginActivity.class );
                    startActivity(intent);
                    finish();
                } else {
                    m_textDetails.setText(setWelcomeMessage(currentProfile));
                    m_profileImage.setProfileId(setProfilePicture(currentProfile));
                }
            }
        };

        m_tokenTracker.startTracking();
        m_profileTracker.startTracking();
        Log.i("savedInstaceState", String.valueOf(savedInstanceState));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        vibrate();
        m_callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        super.onStart();
        m_use_vibrator = m_sp.getBoolean("vibrate", false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Profile profile = Profile.getCurrentProfile();
        m_textDetails.setText(setWelcomeMessage(profile));
        m_profileImage.setProfileId(setProfilePicture(profile));
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        m_tokenTracker.stopTracking();
        m_profileTracker.stopTracking();
    }

    // Set welcome message for the current user
    private String setWelcomeMessage(Profile profile) {
        StringBuilder stringBuffer = new StringBuilder();
        if (profile != null) {
            stringBuffer.append("Welcome ").append(profile.getName());
        }
        return stringBuffer.toString();
    }

    // Set profile picture for the current user (FB profile image)
    private String setProfilePicture(Profile profile) {
        StringBuffer stringBuffer;
        stringBuffer = new StringBuffer();
        if (profile != null) {
            stringBuffer.append(profile.getId());
        }
        return stringBuffer.toString();
    }

    // Check if network is available
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    // hungry button triggers the foodDialogFragment
    public void hungry(View view) {
        if (isNetworkAvailable()) {
            vibrate();
            foodDialogFragment fdf = new foodDialogFragment();
            fdf.show(getFragmentManager(), "FoodDialogFragment");
        }
        else {
            vibrate();
            Toast.makeText(getApplicationContext(), "No network available", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        // if fragment is in use, pop it off the back stack and finish it
        if (getFragmentManager().getBackStackEntryCount() == 0) {
            this.finish();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        vibrate();

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this,SettingsActivity.class);
            startActivity( intent );
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void vibrate() {
        if ( m_use_vibrator ) {
            m_vibrator.vibrate( 300 );
        }
    }
}
