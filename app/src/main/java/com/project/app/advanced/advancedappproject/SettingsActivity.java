package com.project.app.advanced.advancedappproject;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;

/**
 * SettingsActivity used for the settings option in the ActionBar
 */
public class SettingsActivity extends PreferenceActivity {

    private Preference m_logoutPref;

    /**
     * onCreate initializes the activity.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        addPreferencesFromResource(R.xml.preferences);
        m_logoutPref = findPreference("logout");
        logoutListener();
    }


    /**
     * logoutListener hooks up a listener to the logout button.
     */
    public void logoutListener() {
        m_logoutPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Toast.makeText(getApplicationContext(), "Logout!", Toast.LENGTH_LONG).show();
                LoginManager.getInstance().logOut();
                return true;
            }
        });
    }
}

