package com.project.app.advanced.advancedappproject;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * ArrayListFragment is used to display the menu for a restaurant.
 */
public class ArrayListFragment extends ListFragment {
    private String m_sessionID;
    private View m_view;
    private String[] m_names;
    private String[] m_prices;
    private String[] m_descriptions;
    private int m_retry = 0;
    private boolean m_error = false;

    static ArrayListFragment newInstance(String sessionID) {
        ArrayListFragment f = new ArrayListFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString("sessionID", sessionID);
        f.setArguments(args);

        return f;
    }

    /**
     * MenuTask requests the restaurant information for
     * the current session.
     */
    class MenuTask extends AsyncTask<Void, Void, JSONObject> {
        OkHttpClient m_http_client = new OkHttpClient();

        @Override
        protected JSONObject doInBackground(Void... params) {
            Log.v("MenuTask", "Requesting restaurant info");
            Request request = new Request.Builder()
                    .url(Constants.SERVER_URL + "api/session/" + m_sessionID + "/restaurant")
                    .build();

            JSONObject result = null;
            try {
                Response response = m_http_client.newCall(request).execute();
                String res = response.body().string();
                result = new JSONObject(res);

            } catch (IOException | JSONException e) {
                Log.d("MenuTask", e.getMessage());
            }
            return result;
        }
        @Override
        protected void onPostExecute(JSONObject result) {
            if (m_retry == 10){
                View tv = m_view.findViewById(R.id.text);
                ((TextView)tv).setText(R.string.connection_error);
            }
            if (result == null){
                new MenuTask().execute();
                m_retry++;
            }
            else {
                m_retry = 0;
                View tv = m_view.findViewById(R.id.text);
                ListView lv = (ListView) m_view.findViewById(R.id.menuList);
                try {
                    ((TextView) tv).setText(result.getString("name"));
                    List<String> names = new ArrayList<>();
                    List<String> prices = new ArrayList<>();
                    List<String> descriptions = new ArrayList<>();
                    JSONArray jsonSoups = result.getJSONArray("soups");
                    if (jsonSoups != null) {
                        for (int i = 0; i < jsonSoups.length(); i++) {
                            JSONObject row = jsonSoups.getJSONObject(i);
                            names.add(row.getString("name"));
                            prices.add("Price: " + row.getString("price") + " ISK");
                            descriptions.add(row.getString("description"));
                        }
                    }
                    JSONArray jsonStarters = result.getJSONArray("starters");
                    if (jsonStarters != null) {
                        for (int i = 0; i < jsonStarters.length(); i++) {
                            JSONObject row = jsonStarters.getJSONObject(i);
                            names.add(row.getString("name"));
                            prices.add("Price: " + row.getString("price") + " ISK");
                            descriptions.add(row.getString("description"));
                        }
                    }
                    JSONArray jsonMain = result.getJSONArray("main_course");
                    if (jsonMain != null) {
                        for (int i = 0; i < jsonMain.length(); i++) {
                            JSONObject row = jsonMain.getJSONObject(i);
                            names.add(row.getString("name"));
                            prices.add("Price: " + row.getString("price") + " ISK");
                            descriptions.add(row.getString("description"));
                        }
                    }
                    JSONArray jsonDeserts = result.getJSONArray("deserts");
                    if (jsonDeserts != null) {
                        for (int i = 0; i < jsonDeserts.length(); i++) {
                            JSONObject row = jsonDeserts.getJSONObject(i);
                            names.add(row.getString("name"));
                            prices.add("Price: " + row.getString("price") + " ISK");
                            descriptions.add(row.getString("description"));
                        }
                    }
                    JSONArray jsonDrinks = result.getJSONArray("drinks");
                    if (jsonDrinks != null) {
                        for (int i = 0; i < jsonDrinks.length(); i++) {
                            JSONObject row = jsonDrinks.getJSONObject(i);
                            names.add(row.getString("name"));
                            prices.add("Price: " + row.getString("price") + " ISK");
                            descriptions.add(row.getString("description"));
                        }
                    }

                    m_names = new String[names.size()];
                    m_names = names.toArray(m_names);

                    m_prices = new String[prices.size()];
                    m_prices = prices.toArray(m_prices);

                    m_descriptions = new String[descriptions.size()];
                    m_descriptions = descriptions.toArray(m_descriptions);


                } catch (JSONException e) {
                    Log.d("MenuTask", e.getMessage());
                    m_error = true;
                }
                if(!m_error){
                    CustomMenuAdapter m_adapter = new CustomMenuAdapter(getActivity(), m_names, m_prices, m_descriptions);
                    lv.setAdapter(m_adapter);
                }
            }
        }

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get sessionID
        m_sessionID = getArguments() != null ? getArguments().getString("sessionID") : "1";
        if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
            new MenuTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            new MenuTask().execute();
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        m_view = inflater.inflate(R.layout.fragment_pager_list, container, false);
        View tv = m_view.findViewById(R.id.text);
        ((TextView)tv).setText(R.string.finding_menu);
        return m_view;
    }

    @Override
    public void onDestroy() {
        m_error = true;
        super.onDestroy();
    }
}
