package com.project.app.advanced.advancedappproject;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

/**
 * LoginActivity displays the login screen for the user.
 */
public class LoginActivity extends AppCompatActivity {

    private CallbackManager m_callbackManager;
    private AccessTokenTracker m_tokenTracker;
    private ProfileTracker m_profileTracker;
    private Profile m_profile = Profile.getCurrentProfile();
    private String m_sessionID;

    private FacebookCallback<LoginResult> m_callback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(LoginResult loginResult) {
            Log.d("LA", "OnSuccess");

            if (Profile.getCurrentProfile() != null) {
                if(m_sessionID != null && !m_sessionID.isEmpty()){
                    Log.d("LA", "starting Session Activity");
                    Intent intent = new Intent(LoginActivity.this, SessionActivity.class );
                    intent.putExtra("sessionID", m_sessionID);
                    startActivity( intent );
                    finish();
                }
                else {
                    Log.d("LA", "starting Main Activity");
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }

        @Override
        public void onCancel() {
            Log.d("iHungr", "onCancel");
        }

        @Override
        public void onError(FacebookException error) {
            if (!isNetworkAvailable()) {
                Toast.makeText(getApplicationContext(), "No network available", Toast.LENGTH_LONG).show();
            }
            Log.d("iHungr", "onError" + error);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);

        m_sessionID = getIntent().getStringExtra("sessionID");
        Log.d("LA", "Session id: " + m_sessionID);

        if (m_profile != null) {
            if(m_sessionID != null && !m_sessionID.isEmpty()){
                Log.d("LA", "starting Session Activity");
                Intent intent = new Intent(LoginActivity.this, SessionActivity.class );
                intent.putExtra("sessionID", m_sessionID);
                startActivity( intent );
                finish();
            }
            else{
                Intent intent = new Intent(LoginActivity.this, MainActivity.class );
                startActivity( intent );
                finish();
            }
        }

        m_callbackManager = CallbackManager.Factory.create();

        // Setup Facebook login button
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("user_friends");
        loginButton.registerCallback(m_callbackManager, m_callback);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        m_tokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                Log.d("iHungr", "" + currentAccessToken);
            }
        };

        m_profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                if (currentProfile != null) {
                    if(m_sessionID != null && !m_sessionID.isEmpty()){
                        Log.d("LA", "starting Session Activity");
                        Intent intent = new Intent(LoginActivity.this, SessionActivity.class );
                        intent.putExtra("sessionID", m_sessionID);
                        startActivity( intent );
                        finish();
                    }
                    else{
                        Log.d("LA", "starting Main Activity");
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class );
                        startActivity(intent);
                        finish();
                    }

                }
                Log.d("iHungr", "" + currentProfile);
            }
        };

        m_tokenTracker.startTracking();
        m_profileTracker.startTracking();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        m_callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        m_tokenTracker.stopTracking();
        m_profileTracker.stopTracking();
    }

    // Check if network is available
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
