package com.project.app.advanced.advancedappproject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.preference.PreferenceManager;

import java.util.ArrayList;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * foodDialog fragment is an alert dialog where the
 * user can select his food preferences
 */
public class foodDialogFragment extends DialogFragment {
    ArrayList<String> mSelectedItems;
    String[] food;

    private Vibrator m_vibrator;
    private Boolean m_use_vibrator = false;
    SharedPreferences m_sp;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        m_sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        m_vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        m_use_vibrator = m_sp.getBoolean("vibrate", false);
        food = getResources().getStringArray(R.array.foodDialog);
        mSelectedItems = new ArrayList<>();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pick a type of food")
                .setMultiChoiceItems(R.array.foodDialog, null,
                        new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                                if (isChecked) {
                                    vibrate();
                                    // If the user checked the item, add it to the selected items
                                    mSelectedItems.add(food[which]);
                                } else if (mSelectedItems.contains(food[which])) {
                                    vibrate();
                                    // Else, if the item is already in the array, remove it
                                    mSelectedItems.remove(mSelectedItems.indexOf(food[which]));
                                }
                            }
                        })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        vibrate();
                        Intent intent = new Intent(getActivity(), MatchMakingActivity.class );
                        intent.putExtra("foodChoices", mSelectedItems);
                        startActivity(intent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        vibrate();
                    }
                });

        return builder.create();
    }

    public void vibrate() {
        if ( m_use_vibrator ) {
            m_vibrator.vibrate( 300 );
        }
    }
}