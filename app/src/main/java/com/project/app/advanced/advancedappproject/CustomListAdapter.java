package com.project.app.advanced.advancedappproject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Vibrator;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.ProfilePictureView;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * CustomListAdapter is the adapter used to populate the
 * list view containing members in a session
 */
public class CustomListAdapter extends BaseAdapter {

    private final Context m_context;
    private final String[] m_names;
    private final String[] m_pictures;
    private static LayoutInflater m_layoutInflater = null;

    private Vibrator m_vibrator;
    private Boolean m_use_vibrator = false;
    SharedPreferences m_sp;

    public CustomListAdapter(Activity activity, String[] names, String[] pictures) {
        m_context = activity;
        m_names = names;
        m_pictures = pictures;
        m_layoutInflater = (LayoutInflater)m_context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return m_names.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView textView;
        ProfilePictureView profilePictureView;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        m_sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        m_vibrator = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);
        m_use_vibrator = m_sp.getBoolean("vibrate", false);

        Holder holder = new Holder();
        View rowView;
        rowView = m_layoutInflater.inflate(R.layout.friend_list, null);

        holder.textView = (TextView) rowView.findViewById(R.id.name);
        holder.profilePictureView = (ProfilePictureView) rowView.findViewById(R.id.profilePic);
        holder.textView.setText(m_names[position]);
        holder.profilePictureView.setProfileId(m_pictures[position]);

        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate();
                Toast.makeText(m_context, m_names[position], Toast.LENGTH_LONG).show();
            }
        });

        return rowView;
    }

    public void vibrate() {
        if ( m_use_vibrator ) {
            m_vibrator.vibrate( 300 );
        }
    }

}
