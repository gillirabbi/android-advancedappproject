package com.project.app.advanced.advancedappproject;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * SessionFragmentAdapter is used for the three view pagers in
 * the session activity; menu, info and chat.
 */
public class SessionFragmentAdapter extends FragmentPagerAdapter {
    String m_sessionID = "";
    String m_restaurant = "";
    static final int NUM_ITEMS = 3;

    public void upDateSession(String sessionID, String restaurant){
        // get info from MatchMakingActivity
        this.m_sessionID = sessionID;
        this.m_restaurant = restaurant;
    }

    public SessionFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return ArrayListFragment.newInstance(m_sessionID);
            case 1:
                return InfoFragment.newInstance(m_sessionID, m_restaurant);
            case 2:
                return new ChatFragment();
        }
        return null;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Menu";
            case 1:
                return "Info";
            case 2:
                return "Chat";
        }
        return null;
    }

}
